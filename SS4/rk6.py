import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def rk6(F, a, b, ya, n):
    """Solve the first order initial value problem
         y'(t) = F(t, y(t)),
          y(a) = ya,
    using the Runge-Kutta method and return a tuple made of two arrays
    (tarr, yarr) where 'ya' approximates the solution on a uniformly
    spaced grid 'tarr' over [a, b] with n elements.

    Parameters
    ----------
    F : function
         A function of two variables of the form F(t, y), such that
         y'(t) = F(t, y(t)).
    a : float
         Initial time.
    b : float
         Final time.
    n : integer
         Controls the step size of the time grid, h = (b - a) / (n - 1)
    ya : float
         Initial condition at ya = y(a).
    """
    tarr = np.linspace(a, b, n)
    h = tarr[1] - tarr[0]
    ylst = []
    yi = ya
    for t in tarr:
        ylst.append(yi)
        k1 = F(t, yi)
        k2 = F(t + 0.5 * h, yi + 0.5 * h * k1)
        k3 = F(t + 0.5 * h, yi + 0.5 * h * k2)
        k4 = F(t + 0.5 * h, yi + 0.5 * h * k3)
        k5 = F(t + 0.5 * h, yi + 0.5 * h * k4)
        k6 = F(t + h, yi + h * k5)
        yi +=  h / 45.0 * (k1 + 15.0 * k2 + 15.0 * k3 + 9.0 * k4 + 4.0 * k5 + k6)
    yarr = np.array(ylst)
    return tarr, yarr

def interest(ode_solver, n, r=0.1, y0=1000, t0=0.0, t1=5.0):
    """Solve ODE   y'(t) = r y(t), y(0) = y0
    and return the absolute error.

    Parameters
    ----------
    ode_solver : function
         A function ode_solver(F, a, b, ya, n) that solves an explicit initial value problem
         y'(t) = F(t, y(t)) with initla condition y(a) = ya. See 'euler', or 'rk2'.
    n : integer, optional
         The number of time steps
    r : float, optional
         The interest rate
    y0 : float, optional
         The amount of savings at the initial time t0.
    t0 : float, optional
         Initial time.
    t1 : float, optional
         Final time.
    """
    # Exact solution
    t = np.linspace(t0, t1)
    y = y0 * np.exp(r * t)
    y1 = y[-1]
    print "Savings after %d years = %f" %(t1, y1)

    # Plot the exact solution if the figure is empty
    if not plt.gcf().axes:
        plt.plot(t, y, 'o-k', label='exact', lw=4)
        plt.xlabel('Time [years]')
        plt.ylabel(u'Savings [�]')

    # Numerical solution
    F = lambda t, y: r * y  # the function y'(t) = F(t,y)
    tarr, yarr = ode_solver(F, t0, t1, y0, n)
    yarr1 = yarr[-1]
    #abs_err = abs(yarr1 - y1)
    #rel_err = abs_err / y1
    #print "%s steps: estimated savings = %8.6f,"\
     #     " error = %8.6f (%5.6f %%)" % (str(n).rjust(10), yarr1,
      #                                   abs_err, 100 * rel_err)
    plt.plot(tarr, yarr, label='n = %d' % n, lw=2)
    plt.legend(loc=0)
    plt.show()
    return tarr, yarr
#abs_err

interest(rk6, 50)
