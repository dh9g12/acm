import scipy
import numpy
from pprint import pprint
from numpy import array, zeros, diag, diagflat, dot



def isdiagdom(A):
    """Takes a square matrix-like object A as input and returns True
    if matrix A is strictly diagonnally dominant, False otherwise"""
    test=4
    if abs(A[0,0]) < abs(A[0,1]) + abs(A[0,2]):
        test = 0
    elif abs(A[1,1]) < abs(A[1,0]) + abs(A[1,2]):
        test = 0
    elif abs(A[2,2]) < abs(A[2,0]) + abs(A[2,1]):
        test = 0
    else:
        test = 1
    

    if test == 1:
        print 'yes'
    else:
        print 'no'
A = array([[4.0, -1.0, 1.0],[4.0, -8.0, 1.0] , [ -2.0, 1.0, 5.0]])
A2 = array([[ -2.0, 1.0, 5.0], [4.0, -8.0, 1.0] , [4.0, -1.0, 1.0] ])
