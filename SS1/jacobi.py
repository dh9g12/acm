import scipy
import numpy
from pprint import pprint
from numpy import array, zeros, diag, diagflat, dot
test = 0

def jacobi(A,b,N=25,x=None):
    """Solves the equation Ax=b via the Jacobi iterative method."""
    # Create an initial guess if needed
    if x is None:
        x = zeros(len(A[0]))
    
    # Create a vector of the diagonal elements of A
    # and subtract them from A                                                                                                   
    D = diag(A)
    R = A - diagflat(D)
    
    # Iterate for N times                                                                                                                        
    for i in range(N):
        x = (b - dot(R,x))/D
        pprint(x)
    return x

def isdiagdom(A):
    """Takes a square matrix-like object A as input and returns True
    if matrix A is strictly diagonnally dominant, False otherwise"""
    global test
    if abs(A[0,0]) < abs(A[0,1]) + abs(A[0,2]):
        test = 0
    elif abs(A[1,1]) < abs(A[1,0]) + abs(A[1,2]):
        test = 0
    elif abs(A[2,2]) < abs(A[2,0]) + abs(A[2,1]):
        test = 0
    else:
        test = 1

    
# Set up problem here
A = array([[4.0, -1.0, 1.0],[4.0, -8.0, 1.0] , [ -2.0, 1.0, 5.0]])
A2 = array([[ -2.0, 1.0, 5.0], [4.0, -8.0, 1.0] , [4.0, -1.0, 1.0] ])
b = array([15.0 , -21.0, 7.0])
guess = array([1.0,2.0,2.0])

# Solve
isdiagdom(A)
if test == 1:
    sol = jacobi(A,b,N=25,x=guess)
else:
    print 'Matrix is not diagonally dominant'


print "A:"
pprint(A)

print "b:"
pprint(b)

print "x:"
pprint(sol)
