import numpy as np
# Uncomment next two lines for bigger fonts
import matplotlib
size = 20
matplotlib.rcParams['figure.figsize'] = (10, 6)
matplotlib.rcParams['axes.labelsize'] = size
matplotlib.rcParams['axes.titlesize'] = size
matplotlib.rcParams['xtick.labelsize'] = size * 0.6
matplotlib.rcParams['ytick.labelsize'] = size * 0.6
import matplotlib.pyplot as plt

def euler(F, a, b, ya, n):
    """Solve the first order initial value problem
         y'(t) = F(t, y(t)),
          y(a) = ya,
    using Euler's method and return a tuple of made of two arrays (tarr, yarr)
    that approximate the solution on a uniformly spaced grid over [a, b]with n elements.

    Parameters
    ----------
    a : float
         Initial time.
    b : float
         Final time.
    n : integer
         Controls the step size of the time grid, h = (b - a) / (n - 1)
    ya : float
         Initial condition at ya = y(a).


    """
    tarr = np.linspace(a, b, n)
    h = tarr[1] - tarr[0]
    ylst = []
    yi = ya
    for t in tarr:
        ylst.append(yi)
        yi += h * F(t, yi)

    yarr = np.array(ylst)
    return tarr, yarr

t1,y1=euler(lambda t, y: -9.8 * t, 0, 1, 10, 20)
plt.plot(t1,y1)
plt.show()
