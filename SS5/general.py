import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.linalg import block_diag

size = 5
Nx = Ny = size
x = np.linspace(0, 1, Nx)
y = np.linspace(0, 1, Ny)
X, Y = np.meshgrid(x, y)
#print 'x = y = \n', x
#print 'X = \n', X
#print 'Y = \n', Y


# Create a 3x3 array
T = np.arange((size-2)*(size-2)).reshape((size-2),(size-2))
# Flatten the array
U = T.flatten()
# Reshape the array and check that we get back T
Tnew = U.reshape((size-2),(size-2))
assert np.all(Tnew == T)
# Print T, U and Tnew
print "T = \n", T
print "U = T.flatten() = \n", U
print "T = U.reshape(3,3) = \n", Tnew

B = np.array([[-4,  1,  0], [ 1, -4,  1], [ 0,  1, -4]])
db = [B]*(size-2)
A = block_diag(*db)
# Upper diagonal array offset by 3
Dupper = np.diag(np.ones(3 * 2), 3)
# Lower diagonal array offset by -3
Dlower = np.diag(np.ones(3 * 2), -3)
A += Dupper + Dlower
print "A = \n", A
