import numpy as np
def euler(F, a, b, ya, n):
    """Solve the first order initial value problem
         y'(t) = F(t, y(t)),
          y(a) = ya,
    using Euler's method and return a tuple of made of two arrays (tarr, yarr)
    that approximate the solution on a uniformly spaced grid over [a, b]with n elements.

    Parameters
    ----------
    a : float
         Initial time.
    b : float
         Final time.
    n : integer
         Controls the step size of the time grid, h = (b - a) / (n - 1)
    ya : float
         Initial condition at ya = y(a).


    """
    tarr = np.linspace(a, b, n)
    h = tarr[1] - tarr[0]
    ylst = []
    yi = ya
    for t in tarr:
        ylst.append(yi)
        yi += h * F(t, yi)

    yarr = np.array(ylst)
    #return tarr, yarr
    print(tarr)
    print(yarr)

g = 9.8
h = 10
t0 = 0.0
t1 = 1.0  # change to 20
t = np.linspace(t0, t1)
y = -0.5*g*t**2 + h

euler(lambda t, y: -g * t, t0, t1, h, 100)
