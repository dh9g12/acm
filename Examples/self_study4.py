import numpy as np
import matplotlib
size = 20
matplotlib.rcParams['figure.figsize'] = (10, 6)
matplotlib.rcParams['axes.labelsize'] = size
matplotlib.rcParams['axes.titlesize'] = size
matplotlib.rcParams['xtick.labelsize'] = size * 0.6
matplotlib.rcParams['ytick.labelsize'] = size * 0.6
import matplotlib.pyplot as plt

def euler(F, a, b, ya, n):
    """Solve the first order initial value problem
         y'(t) = F(t, y(t)),
          y(a) = ya,
    using Euler's method and return a tuple of made of two arrays (tarr, yarr)
    that approximate the solution on a uniformly spaced grid over [a, b]with n elements.

    Parameters
    ----------
    a : float
         Initial time.
    b : float
         Final time.
    n : integer
         Controls the step size of the time grid, h = (b - a) / (n - 1)
    ya : float
         Initial condition at ya = y(a).


    """
    tarr = np.linspace(a, b, n)
    h = tarr[1] - tarr[0]
    ylst = []
    yi = ya
    for t in tarr:
        ylst.append(yi)
        yi += h * F(t, yi)

    yarr = np.array(ylst)
    return tarr, yarr


def rk4(F, a, b, ya, n):
    """Solve the first order initial value problem
         y'(t) = F(t, y(t)),
          y(a) = ya,
    using the Runge-Kutta method and return a tuple made of two arrays
    (tarr, yarr) where 'ya' approximates the solution on a uniformly
    spaced grid 'tarr' over [a, b] with n elements.

    Parameters
    ----------
    F : function
         A function of two variables of the form F(t, y), such that
         y'(t) = F(t, y(t)).
    a : float
         Initial time.
    b : float
         Final time.
    n : integer
         Controls the step size of the time grid, h = (b - a) / (n - 1)
    ya : float
         Initial condition at ya = y(a).
    """
    tarr = np.linspace(a, b, n)
    h = tarr[1] - tarr[0]
    ylst = []
    yi = ya
    for t in tarr:
        ylst.append(yi)
        k1 = F(t, yi)
        k2 = F(t + 0.5 * h, yi + 0.5 * h * k1)
        k3 = F(t + 0.5 * h, yi + 0.5 * h * k2)
        k4 = F(t + h, yi + h * k3)
        yi +=  h / 6.0 * (k1 + 2.0 * k2 + 2.0 * k3 + k4)
    yarr = np.array(ylst)
    return tarr, yarr
t1,y1 = euler(lambda t, y: -15 * y, 0, 1, 1, 20)
plt.plot(t1, y1, 'k-o', label='euler')
t2,y2 = rk4(lambda t, y: -15 * y, 0, 1, 1, 20)
plt.plot(t2, y2, '--', label='rk4')
plt.show()

