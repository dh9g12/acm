import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def euler(F, a, b, ya, n):
    """Solve the first order initial value problem
         y'(t) = F(t, y(t)),
          y(a) = ya,
    using Euler's method and return a tuple of made of two arrays (tarr, yarr)
    that approximate the solution on a uniformly spaced grid over [a, b]with n elements.

    Parameters
    ----------
    a : float
         Initial time.
    b : float
         Final time.
    n : integer
         Controls the step size of the time grid, h = (b - a) / (n - 1)
    ya : float
         Initial condition at ya = y(a).


    """
    tarr = np.linspace(a, b, n)
    h = tarr[1] - tarr[0]
    ylst = []
    yi = ya
    for t in tarr:
        ylst.append(yi)
        yi += h * F(t, yi)

    yarr = np.array(ylst)
    return tarr, yarr

# Exact solution
r = 0.1
y0 = 1000
t0 = 0.0
t1 = 5.0  # change to 20
t = np.linspace(t0, t1)
y = y0 * np.exp(r * t)
y1 = y[-1]
print "Savings after %d years = %f" %(t1, y1)

# Numerical solutions with Euler's method
plt.plot(t, y, 'o-k', label='exact', lw=4)
plt.xlabel('Time [years]')
plt.ylabel(u'Savings [�]')
nbsteps = [5, 10, 20, 50, 100, 1000, 10000]
errlst = []
for n in nbsteps:
    teuler, yeuler = euler(lambda t, y: r * y, t0, t1, y0, n)
    yeuler1 = yeuler[-1]
    abs_err = abs(yeuler1 - y1)
    rel_err = abs_err / y1
    print "%s steps: estimated savings = %8.3f, error = %8.3f (%5.3f %%)" % (str(n).rjust(10), yeuler1, abs_err, 100 * rel_err)
    plt.plot(teuler, yeuler, label='n = %d' % n, lw=2)
    errlst.append(abs_err)
#plt.axis([4, 5, 1400, 1700])
plt.legend(loc=0)
