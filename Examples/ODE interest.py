# -*- coding: cp1252 -*-
# Setup notebook
#%matplotlib inline
import numpy as np
# Uncomment next two lines for bigger fonts
import matplotlib
size = 20
matplotlib.rcParams['figure.figsize'] = (10, 6)
matplotlib.rcParams['axes.labelsize'] = size
matplotlib.rcParams['axes.titlesize'] = size
matplotlib.rcParams['xtick.labelsize'] = size * 0.6
matplotlib.rcParams['ytick.labelsize'] = size * 0.6
import matplotlib.pyplot as plt

m = 1
h = 10
g = 9.8
tmax = 1.5
t = np.linspace(0, tmax)
y = -0.5 * g * t**2 + h
plt.plot(t, y)
plt.xlabel('Time [s]')
plt.ylabel('Altitude [m]')
plt.show()
