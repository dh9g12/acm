import numpy as np
## example2_17
def iterate(x, omega=1.0):
    """Use the Gauss-Seidel algorithm to iterate the estimated solution
    vector x to equation
         A x = b,
    and return the improved solution.

    x : array of floats of size n
         Solution vector.
    omega : float
         Relaxation factor.

    """
    n = len(x)
    x[0] = omega*(x[1] - x[n - 1]) / 2.0 + (1.0 - omega) * x[0]
    for i in range(1, n - 1):
        x[i] = omega * (x[i - 1] + x[i + 1]) / 2.0 + (1.0 - omega) * x[i]
    x[n - 1] = omega * (1.0 - x[0] + x[n - 2]) / 2.0 + (1.0 - omega) * x[n - 1]
    return x

