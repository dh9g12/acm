import numpy as np

def ispositive(A):
    """Return True if the input matrix is positive-definite"""
    return np.all(np.linalg.eigvals(A)>0)


A = np.array([[1.0, 0.0],[0.0, 1.0]])
A2 = np.array([[ -2.0, 1.0, 5.0], [4.0, -8.0, 1.0] , [4.0, -1.0, 1.0] ])
