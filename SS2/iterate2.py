from numpy import dot, sqrt, zeros

def iterate2(x, omega=1):
    n = len(x)
    x[0] = omega*(x[1] - x[n - 1]) / 4.0 + (1.0 - omega) * x[0]
    for i in range(1, n - 1):
        x[i] = omega * (x[i - 1] + x[i + 1]) / 4.0 + (1.0 - omega) * x[i]
    x[n - 1] = omega * (100.0 - x[0] + x[n - 2]) / 4.0 + (1.0 - omega) * x[n - 1]
    return x

def gauss_seidel(iterate, x, tol=1.0e-9, relaxation=False):
    """ x, niter, omega = gauss_seidel(iterate, x, tol=1.0e-9, omega=1.0)

    Gauss-Seidel method for solving [A]{x} = {b}.

    The matrix [A] should be sparse. User must supply the
    function iterate(x, omega) that returns the improved {x},
    given the current {x}. 'omega' is the relaxation factor.
    """
    omega = 1.0
    k = 10
    p = 1
    for i in range(1,501):
        xold = x.copy()
        x = iterate(x, omega)
        dx = sqrt(dot(x - xold, x - xold))
        if dx < tol:
            return x, i, omega
        if relaxation:
            # Compute of relaxation factor after k+p iterations
            if i == k:
                dx1 = dx
            if i == k + p:
                dx2 = dx
                omega = 2.0 / (1.0 + sqrt(1.0 - (dx2 / dx1)**(1.0 / p)))
    print 'Gauss-Seidel failed to converge'

size = 20
x = zeros(size)
x, niter, omega = gauss_seidel(iterate2,x)
print "\nNumber of iterations =", niter
print "\nRelaxation factor =", omega
print "\nThe solution is:\n", x
