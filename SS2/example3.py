from numpy import zeros

def get_matrix_A(n):
    A=zeros(shape=(n,n))
    A[0,0]=2
    A[0,1]=-1
    A[0,n-1]=1
    A[n-1,0]=1
    A[n-1,n-2]=-1
    A[n-1,n-1]=2
    for i in range(1,n-1):
        A[i,i-1]=-1
        A[i,i+1]=-1
        A[i,i]=2
    return A

def get_vector_b(n):
    b=zeros(n)
    b[n-1]=1
    return b
