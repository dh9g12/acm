from pprint import pprint
from numpy import array, zeros, diag, diagflat, dot

def get_matrix_A(n):
    A=zeros(shape=(n,n))
    A[0,0]=2
    A[0,1]=-1
    A[0,n-1]=1
    A[n-1,0]=1
    A[n-1,n-2]=-1
    A[n-1,n-1]=2
    for i in range(1,n-1):
        A[i,i-1]=-1
        A[i,i+1]=-1
        A[i,i]=2
    return A

def get_vector_b(n):
    b=zeros(n)
    b[n-1]=1
    return b


def jacobi(n):
    """Solves the equation Ax=b via the Jacobi iterative method."""
    # Create an initial guess if needed
    A = get_matrix_A(n)
    b = get_vector_b(n)
    x = zeros(n)
    

    # Create a vector of the diagonal elements of A
    # and subtract them from A
    D = diag(A)
    R = A - diagflat(D)

    # Iterate for N times
    for i in range(25):
        x = (b - dot(R,x))/D

    return x

# Set up problem here


# Solve


