import numpy as np
import scipy as sp
import scipy.linalg
import matplotlib
size = 20
matplotlib.rcParams['figure.figsize'] = (10, 6)
matplotlib.rcParams['axes.labelsize'] = size
matplotlib.rcParams['axes.titlesize'] = size
matplotlib.rcParams['xtick.labelsize'] = size * 0.6
matplotlib.rcParams['ytick.labelsize'] = size * 0.6
import matplotlib.pyplot as plt

def embed(T, Te=100):
    """Embed the array T giving the temperature at the inner nodes in
    the domain into a larger array including the boundary temperatures
    """
    N = T.shape[0] + 2
    Tfull = np.zeros((N,N))
    Tfull[0] = Te
    Tfull[1:-1, 1:-1] = T
    return Tfull

def laplace2d(get_A, get_b, N=50, Te=100):
    """Solve the Laplace equation on a 2D grid, with T=0 at all
    boundaries except y=0, where T=Te, and return an 2D array of size
    NxN giving the temperature distribution throughout the domain.
    """
    n = N - 2
    A = get_A(n)
    b = get_b(n, Te)
    U = sp.linalg.solve(A, b)
    T = U.reshape((n, n))
    Tfull = embed(T, Te)
    return Tfull

def plot_pcolor(Tfull):
    """Plot temperature in the domain using pcolor"""
    N = Tfull.shape[0]
    x = y = np.linspace(0, 1, N)
    X, Y = np.meshgrid(x,y)
    plt.pcolor(X, Y, Tfull)
    plt.axis('scaled')
    plt.colorbar()
    plt.xlabel('x (m)')
    plt.ylabel('y (m)')
    plt.title('T(x,y) on %dx%d grid' % (N,N))
    plt.show()

def get_k(i, j, n):
    """Convert from (i,j) indices in T array to k index in U column vector.
    """
    return i * n + j

def move(i, j, direction):
    """Move from index (i,j) in direction 'up', 'down', 'left' or 'right'.
    """
    if direction == 'up':
        return i - 1, j
    if direction == 'down':
        return i + 1, j
    if direction == 'left':
        return i, j - 1
    if direction == 'right':
        return i, j + 1
    # Unknown direction
    raise ValueError("Unknown direction %s. Usage: move(i,j,direction)\n with direction in ['up','down','left','right']" % direction)

def get_k_neighbours(i, j, n):
    """Return indices of neighbours (k_up, k_right, k_down, k_left)
    going clockwise from the above neighbour.
    """
    klst = []
    for direction in ['up', 'right', 'down', 'left']:
        idir, jdir = move(i, j, direction)
        kdir = get_k(idir, jdir, n)
        klst.append(kdir)
    return klst

def fill(A, ilst=None, jlst=None, directions='UDLR'):
    """Fill the stencil coefficients in matrix A corresponding to
    nodes (i,j), where i is taken from 'ilst' and j from 'jlst', but
    only for the directions listed in string directions, where
    U -> up
    D -> down
    L -> left
    R -> right.
    """
    # A is of shape n^2 x n^2 so:
    n = int(np.sqrt(A.shape[0]))
    # Set default values for ilst and jlst to cover
    # all non-boundary nodes
    if ilst is None:
        # all rows except first and last one
        ilst = range(1, n - 1)
    if jlst is None:
        # all columns except first and last one
        jlst = range(1, n - 1)  #

    # Loop over all nodes (i,j) in [ilst x jlst] and fill A
    for i in ilst:
        for j in jlst:
            k = get_k(i, j, n)
            up, right, down, left =  get_k_neighbours(i, j, n)
            A[k, k]=-4
            if 'U' in directions:
                A[k, up] = 1
            if 'D' in directions:
                A[k, down] = 1
            if 'L' in directions:
                A[k, left] = 1
            if 'R' in directions:
                A[k, right] = 1

def get_A2(n):
    """Return 2D Laplace matrix A using solution 2"""
    # Initialize the matrix
    A = np.zeros((n**2, n**2))
    # Fill A for inner nodes
    fill(A)
    # Fill top, bottom, left and right boundaries:
    fill(A, ilst=[0], directions='DLR')    # top
    fill(A, ilst=[n-1], directions='ULR')  # bottom
    fill(A, jlst=[0], directions='UDR')  # left
    fill(A, jlst=[n-1], directions='UDL')  # right
    # Fill corners
    fill(A, [0], [0], 'RD')      # top left
    fill(A, [0], [n-1], 'LD')    # top right
    fill(A, [n-1], [0], 'UR')    # bottom left
    fill(A, [n-1], [n-1], 'LU')  # bottom right
    return A

def get_b(n, Te=100):
    """Return column vector of size n^2 containing the boundary conditions."""
    b = np.zeros(n**2)
    b[:n] = -Te
    return b

Tfull = laplace2d(get_A2, get_b, N=70)
plot_pcolor(Tfull)


